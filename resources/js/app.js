
require('./bootstrap');

import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);

window.Vue = require('vue');

//COMPOMENT DEFINER//
//MISC COMPONENT

//tambah penguna 
Vue.component('tambah-pengguna',require('./components/AdminOperator/pengguna/tambah.vue'));

//admin operator
Vue.component('data-pengguna',require('./components/AdminOperator/pengguna/data.vue'));
//industri
Vue.component('industri-job',require('./components/AdminOperator/industry/industri.vue'));
//tipe pek
Vue.component('tipe-peker',require('./components/AdminOperator/job_tipe/tipe.vue'));
//gaji pek
Vue.component('gaji-peker',require('./components/AdminOperator/gaji/gaji.vue'));
//pengalaman
Vue.component('pengalaman-p',require('./components/AdminOperator/pengalaman/pengalaman.vue'));
//Keahlian
Vue.component('keahlian-p',require('./components/AdminOperator/keahlian/keahlian.vue'))
//LOWONGAN
Vue.component('kategori-job',require('./components/AdminOperator/lowongan/katgeri.vue'));
Vue.component('data-job',require('./components/AdminOperator/lowongan/data.vue'));
//KUALIFIKASI
Vue.component('kualifikasi-job',require('./components/AdminOperator/kualifikasi/qualification.vue'));

//WILAYAH
Vue.component('wilayah-prov',require('./components/AdminOperator/wilayah/provinsi.vue'));
Vue.component('wilayah-kab',require('./components/AdminOperator/wilayah/kabupaten.vue'));
Vue.component('wilayah-kec',require('./components/AdminOperator/wilayah/kecamatan.vue'));
Vue.component('wilayah-desa',require('./components/AdminOperator/wilayah/desa.vue'));
const app = new Vue({
    el: '#app'
});
