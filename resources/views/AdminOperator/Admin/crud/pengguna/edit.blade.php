@extends('layouts.AO_base')

@section('content')
<div class="content">
    <div class="row">
    	<div class="col-lg-12">
    		<div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10">Pengguna</h1>
                        <h2 class="h4 font-w400 text-white-op">Edit</h2>
                    </div>
                </div>
            </div>
	        <!-- Default Elements -->
	        <div class="block block-rounded">
	            <div class="block-header">
                    <h3 class="block-title"></h3>
                </div>
	            <div class="block-content">
	                <form action="{{route('update_pengguna',$data->id)}}" method="post">
                        @csrf
	                	<div class="row justify-content-center">
	                		<div class="col-md-6">
	                			<input type="hidden" name="id" value="">
	                			<div class="form-group row">
		                            <label class="col-lg-4 col-form-label">Nama Lengkap</label>
		                            <div class="col-lg-8">
		                                <input type="text" id="nama" class="form-control" name="nama" value="{{$data->nama}}"  placeholder="Nama Lengkap">
		                                <div class="form-text text-danger"></div>
		                            </div>
		                        </div>
		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Username</label>
		                            <div class="col-lg-8">
		                                <input type="text" id="username" class="form-control" name="username" value="{{$data->username}}"  placeholder="Username">
		                                <div class="form-text text-danger"></div>
		                            </div>
		                        </div>
		                         <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Email</label>
		                            <div class="col-lg-8">
		                                <input type="email" id="email" class="form-control" name="email" value="{{$data->email}}"  placeholder="Email">
		                                <div class="form-text text-danger"></div>
		                            </div>
		                        </div>
		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Jabatan</label>
		                            <div class="col-lg-8">
	                                    <select class="form-control" name="id_akses">
					                        @foreach($user_acces as $acc)
                                                <option value="{{$acc->id}}">{{$acc->nama}}</option>
                                            @endforeach
		                                </select>
		                                <div class="form-text text-danger"></div>
	                                </div>
		                        </div>
		                    	<div class="row justify-content-center" style="padding-top: 30px;padding-bottom: 25px;">
		                            <div class="col-lg-6">
		                                <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
		                            </div>
		                            <div class="col-lg-6">
		                                <a class="btn btn-danger btn-lg btn-block" href="{{route('show_pengguna')}}">Batal</a>
		                            </div>
			                    </div>
	                		</div>
	                    </div>
                    </form>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>
@endsection