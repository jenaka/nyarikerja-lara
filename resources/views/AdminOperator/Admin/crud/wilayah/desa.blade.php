@extends('layouts.AO_base')

@section('content')
    <div id="app">
        <wilayah-desa :api_token="'{{Auth::user()->api_token}}'" :datid="'{{$datid}}'"></wilayah-desa>
    </div>
@endsection