@extends('layouts.AO_base')

@section('content')
<div class="content">
    <div class="row">
    	<div class="col-lg-12">
    		<div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10">Lowongan Kerja</h1>
                        <h2 class="h4 font-w400 text-white-op">Tambah Lowongan</h2>
                    </div>
                </div>
            </div>
	        <!-- Default Elements -->
	        <div class="block block-rounded">
	            <div class="block-header">
                    <h3 class="block-title"></h3>
                </div>
	            <div class="block-content">

	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>
@endsection