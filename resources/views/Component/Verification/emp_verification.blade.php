@extends('layouts.misc.ver')

@section('content')
    <!-- Page Content -->
    <div class="content content-full">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="js-wizard-validation-classic block">
                    <!-- Step Tabs -->
                    <ul class="nav nav-tabs nav-tabs-block nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#wizard-validation-classic-step1" data-toggle="tab">1. Informasi Penanggung Jawab</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-classic-step2" data-toggle="tab">2. Informasi Perusahaan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-classic-step3" data-toggle="tab">3. Logo Perusahaan</a>
                        </li>
                    </ul>
                    <!-- END Step Tabs -->
                
                    <!-- Form -->
                    <form class="js-wizard-validation-classic-form" action="{{route('val_post')}}" method="post">
                        @csrf
                        <!-- Steps Content -->
                        <div class="block-content block-content-full tab-content" style="min-height: 265px;">

                            <!-- Step 1 -->
                            <div class="tab-pane active" id="wizard-validation-classic-step1" role="tabpanel">
                                <div class="form-group">
                                    <label for="wizard-validation-classic-firstname">Nama Lengkap</label>
                                    <input type="hidden" name="status" value=1>
                                    <input class="form-control" type="text" id="nama_lengkap" name="nama_lengkap" value="{{Auth::user()->nama_lengkap}}">
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-lastname">No KTP</label>
                                    <input class="form-control" type="text" id="no_ktp" name="no_ktp" placeholder="Masukan No. KTP..">
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-email">No. HP</label>
                                    <input class="form-control" type="text" id="no_hp" name="no_hp" placeholder="Masukan No. HP">
                                </div>
                            </div>
                            <!-- END Step 1 -->

                            <!-- Step 2 -->
                            <div class="tab-pane" id="wizard-validation-classic-step2" role="tabpanel">
                                <input type="hidden" name="id_perusahaan" value="{{Auth::user()->id}}">
                       
                                <div class="form-group">
                                    <label for="wizard-validation-classic-firstname">Nama Perushaan</label>
                                    <input class="form-control" type="text" id="nama_perusahaan" name="nama_perusahaan" placeholder="Masukan Nama Perusahaan..">
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-lastname">Industri Perusahaan</label>
                                    <select  id="id_jenis" name="id_jenis" style="width: 100%;" data-placeholder="Pilih..">
                                        <option></option>
                                        @foreach($indu as $ase)
                                            <option value="{{$ase->id}}">{{$ase->nama_industri}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-email">Web Perusahaan</label>
                                    <input class="form-control" type="text" id="web_perusahaan" name="web_perusahaan" placeholder="Masukan Web Perusahaan (Optional)">
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-lastname">Provinsi</label>
                                    <select class="form-control" id="id_provinsi" name="id_provinsi" style="width: 100%;" data-placeholder="Pilih..">
                                        <option></option>
                                        @foreach($prov as $provi)
                                            <option value="{{$provi->id}}">{{$provi->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-lastname">Kota</label>
                                    <select class="form-control" id="id_kota" name="id_kota" style="width: 100%;" data-placeholder="Pilih..">
                                        <option></option> 
                                        @foreach($city as $cit)
                                            <option value="{{$cit->id}}">{{$cit->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-email">Alamat Lengkap</label>
                                    <input class="form-control" type="text" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Masukan Alamat Lengkap Perusahaan">
                                </div>
                                <div class="form-group">
                                    <label for="wizard-validation-classic-email">Deskripsi Perusahaan</label>
                                    <textarea class="js-summernote form-control" name="deskripsi_perusahaan" placeholder="Masukan Deskripsi Perusahaan.."></textarea>
                                </div>
                            </div>
                            <!-- END Step 2 -->

                            <!-- Step 3 -->
                            <div class="tab-pane" id="wizard-validation-classic-step3" role="tabpanel">
                                <div class="form-group row">
                                    
                                </div>
                                <div class="form-group">
                                    <input type="file" name="logo_perusahaan" class="form-control">
                                </div>
                            </div>
                            <!-- END Step 3 -->
                        </div>
                        <!-- END Steps Content -->

                        <!-- Steps Navigation -->
                        <div class="block-content block-content-sm block-content-full bg-body-light">
                            <div class="row">
                                <div class="col-6">
                                    <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                        <i class="fa fa-angle-left mr-5"></i> Sebelumnya
                                    </button>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                        Selanjutnya <i class="fa fa-angle-right ml-5"></i>
                                    </button>
                                    <button type="submit" class="btn btn-alt-primary d-none" data-wizard="finish">
                                        <i class="fa fa-check mr-5"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- END Steps Navigation -->
                    </form>
                    <!-- END Form -->
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection