@extends('layouts.base')

@section('sidebar')
@endsection

@section('header')
@endsection

@section('content')
<div class="bg-white">
    <div class="hero-static content content-full bg-white invisible" data-toggle="appear">
        <!-- Header -->
        <div class="text-center">
            <h3 class="h2 font-w700 mt-50 mb-10">Daftar Pencari Kerja</h3>
        </div>
        <!-- END Header -->

        <!-- Sign Up Form -->
        <div class="row justify-content-center">
            <div class="col-sm-8 col-md-6 col-xl-4">
                <!-- jQuery Validation (.js-validation-signup class is initialized in js/pages/op_auth_signup.js) -->
                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                <form class="js-validation-signup" action="{{route('seeker_save')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="text" class="form-control" id="signup-username" name="signup-username">
                                <label for="signup-username">Nama Lengkap</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="email" class="form-control" id="signup-email" name="signup-email">
                                <label for="signup-email">Email</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="password" class="form-control" id="signup-password" name="signup-password">
                                <label for="signup-password">Password</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="password" class="form-control" id="signup-password-confirm" name="signup-password-confirm">
                                <label for="signup-password-confirm">Konfirmasi Password</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row gutters-tiny">
                        <div class="col-12 mb-10">
                            <button type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-primary">
                                <i class="si si-user-follow mr-10"></i> Daftar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Sign Up Form -->
    </div>
</div>
<script src="{{asset('js/pages/op_auth_signup.js')}}"></script>
@endsection