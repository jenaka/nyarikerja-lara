<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Codebase - Bootstrap 4 Admin Template &amp; UI Framework</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicons/favicon.png')}}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/favicons/favicon-192x192.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicons/apple-touch-icon-180x180.png')}}">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{asset('css/codebase.css')}}">


        <link rel="stylesheet" href="{{asset('js/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('js/plugins/select2/select2-bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('js/plugins/slick/slick.min.css')}}">
        <link rel="stylesheet" href="{{asset('js/plugins/slick/slick-theme.min.css')}}">

        <!-- Codebase Core JS -->
        <script src="{{asset('js/core/jquery.min.js')}}"></script>
        <script src="{{asset('js/core/bootstrap.bundle.min.js')}}"></script>
    </head>
    <body>
        <div id="page-container" class="sidebar-inverse side-scroll page-header-fixed page-header-inverse main-content-boxed">
            <!-- Sidebar -->
            <nav id="sidebar">
                @yield('sidebar')
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        
                        <!-- Side Header -->
                        <div class="content-header content-header-fullrow bg-black-op-10">
                            <div class="content-header-section text-center align-parent">
                                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                    <i class="fa fa-times text-danger"></i>
                                </button>
                                <!-- END Close Sidebar -->

                                <!-- Logo -->
                                <div class="content-header-item">
                                    <a class="font-w700" href="">
                                        <img src="asset('img/logo_white.png')" width="130px">
                                    </a>
                                </div>
                                <!-- END Logo -->
                            </div>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Main Navigation -->
                        <div class="content-side content-side-full">
                            <ul class="nav-main">
                                <li>
                                    <a class="active" href="bd_dashboard.html"><i class="si si-compass"></i>Beranda</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END Side Main Navigation -->

                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                @yield('header')
                <!-- Header Content -->
                <div class="content-header">
                    
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <!-- Logo -->
                        <div class="content-header-item">
                            <a class="font-w700" href="">
                                <img src="{{asset('img/logo_white.png')}}" width="130px">
                            </a>
                        </div>
                        <!-- END Logo -->
                    </div>
                    <!-- END Left Section -->

                    <!-- Middle Section -->
                    <div class="content-header-section d-none d-lg-block">
                        <!-- Header Navigation -->
                        <ul class="nav-main-header">
                            <li>
                                <a href="#"></i>Cari Lowongan Kerja</a>
                            </li>
                            <li>
                                <a href="">Perusahaan</a>
                            </li>
                            <li>
                                <a href="#">Pasang Lowongan</a>
                            </li>
                            <li>
                                <a href="#"></i>Kontak</a>
                            </li>
                        </ul>
                        <!-- END Header Navigation -->
                    </div>
                    <!-- END Middle Section -->

                    <div class="content-header-section">
                        <a href="{{route('loginSec')}}" class="btn btn-sm btn-outline-primary">Login
                        </a>
                        
                        <a href="{{route('daftar')}}" class="btn btn-sm btn-outline-primary">Daftar
                        </a>

                        <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon"></i>
                        </button>
                    </div>
                </div>
                <!-- END Header Content -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                @yield('content')
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                        Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="https://goo.gl/po9Usv" target="_blank">Codebase 2.0</a> &copy; <span class="js-year-copy">2017</span>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        
        <script src="{{asset('js/core/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('js/core/jquery.scrollLock.min.js')}}"></script>
        <script src="{{asset('js/core/jquery.appear.min.js')}}"></script>
        <script src="{{asset('js/core/jquery.countTo.min.js')}}"></script>
        <script src="{{asset('js/core/js.cookie.min.js')}}"></script>
        <script src="{{asset('js/codebase.js')}}"></script>
        <script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>
        <script src="{{asset('js/plugins/slick/slick.min.js')}}"></script>
        <!-- Page JS Plugins -->
        <script src="{{asset('js/plugins/chartjs/Chart.bundle.min.js')}}"></script>
        <script src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
        <!-- Page JS Code -->
        <script src="{{asset('js/pages/be_pages_dashboard.js')}}"></script>
        <script>
            jQuery(function () {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker', 'select2', 'slick']);
            });
        </script>
    </body>
</html>