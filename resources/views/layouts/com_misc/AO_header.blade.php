<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
    @if(Auth::user()->id_akses == 1)
        <title>Pintasku Admin Panel</title>
    @else
        <title>Pintasku Operator Panel</title>
    @endif
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

<meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">

<!-- Open Graph Meta -->
<meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
<meta property="og:site_name" content="Codebase">
<meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
<meta property="og:type" content="website">
<meta property="og:url" content="">
<meta property="og:image" content="">

<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="{{asset('img/favicons/favicon.png')}}">
<link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/favicons/favicon-192x192.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicons/apple-touch-icon-180x180.png')}}">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/select2/select2-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/jquery-auto-complete/jquery.auto-complete.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/jquery-auto-complete/jquery.auto-complete.min.css')}}">

<!-- Codebase framework -->
<link rel="stylesheet" id="css-main" href="{{asset('css/codebase.min.css')}}">

<script src="{{asset('js/core/jquery.min.js')}}"></script>
<script src="{{asset('js/core/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<link rel="stylesheet" href="{{asset('js/plugins/magnific-popup/magnific-popup.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />
<script src="{{asset('js/codebase.js')}}"></script> 
<!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
<!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
<!-- END Stylesheets -->