<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="content-header content-header-fullrow px-15" style="background-color: #2d3238;">
                <!-- Mini Mode -->
                <div class="content-header-section sidebar-mini-visible-b">
                    <!-- Logo -->
                    <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                        <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                    </span>
                    <!-- END Logo -->
                </div>
                <!-- END Mini Mode -->

                <!-- Normal Mode -->
                <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                    <!-- Close Sidebar, Visible only on mobile screens -->
                    <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times text-danger"></i>
                    </button>
                    <!-- END Close Sidebar -->

                    <!-- Logo -->
                    <div class="content-header-item" >
                        <a class="link-effect font-w700" href="{{route('home')}}">
                            <i class="si si-fire text-primary"></i>
                            <span class="font-size-xl text-dual-primary-dark"></span><span class="font-size-xl text-primary">Admin Panel</span>
                        </a>
                    </div>
                    <!-- END Logo -->
                </div>
                <!-- END Normal Mode -->
            </div>
            <!-- END Side Header -->

            <!-- Side User -->
            
            <!-- END Side User -->

            <!-- Side Navigation -->
            <div class="content-side content-side-full">
                <ul class="nav-main">
                    <li>
                        <a href="" class=""><i class="si si-compass"></i><span class="sidebar-mini-hide">Beranda</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-doc"></i><span class="sidebar-mini-hide">Lowongan Kerja</span></a>
                        <ul>
                            <li>
                                <a href="{{route('tambah_lowongan')}}">Tambah Lowongan Kerja</a>
                            </li>
                            <li>
                                <a href="{{route('data_loker')}}">Data Lowongan kerja</a>
                            </li>
                            <li>
                                <a href="{{route('kategori_job')}}">Kategori</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-briefcase"></i><span class="sidebar-mini-hide">Perusahaan</span></a>
                        <ul>
                            <li>
                                <a href="">Tambah Perusahaan</a>
                            </li>
                            <li>
                                <a href="">Data Perusahaan</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Job Seeker</span></a>
                        <ul>
                            <li>
                                <a href="">Tambah Job Seeker</a>
                            </li>
                            <li>
                                <a href="">Data Job Seeker</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('industri')}}" class=""><i class="si si-directions"></i><span class="sidebar-mini-hide">Industri</span></a>
                    </li>
                    <li>
                        <a href="{{route('tipe')}}" class=""><i class="si si-directions"></i><span class="sidebar-mini-hide">Tipe Pekerjaan</span></a>
                    </li>
                    <li>
                        <a href="{{route('gaji')}}" class=""><i class="si si-directions"></i><span class="sidebar-mini-hide">Gaji Pekerjaan</span></a>
                    </li>
                    <li>
                        <a href="{{route('pengalaman')}}" class=""><i class="si si-directions"></i><span class="sidebar-mini-hide">Pengalaman</span></a>
                    </li>
                    <li>
                        <a href="{{route('keahlian')}}" class=""><i class="si si-directions"></i><span class="sidebar-mini-hide">Keahlian</span></a>
                    </li>
                    <li>
                        <a href="{{route('kualifikasi')}}" class=""><i class="si si-graduation"></i><span class="sidebar-mini-hide">Kualifikasi</span></a>
                    </li>
                    <li>
                        <a href="{{route('wilayah')}}" class=""><i class="si si-compass"></i><span class="sidebar-mini-hide">Wilayah</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-user"></i><span class="sidebar-mini-hide">Pengguna</span></a>
                        <ul>
                            <li>
                                <a href="{{route('tambah_pengguna_p')}}">Tambah Pengguna</a>
                            </li>
                            <li>
                                <a href="{{route('show_pengguna')}}">Data Pengguna</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="" class=""><i class="si si-envelope-letter"></i><span class="sidebar-mini-hide">Kontak Pesan</span></a>
                    </li>
                    <li>
                        <a href="" class=""><i class="fa fa-gear"></i><span class="sidebar-mini-hide">Pengaturan</span></a>
                    </li>
                </ul>
            </div>
            <!-- END Side Navigation -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>