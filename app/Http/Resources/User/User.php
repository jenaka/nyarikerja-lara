<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User\Data\Res_Acces;
class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'nama'              => $this->nama,
            'username'          => $this->username,
            'email'             => $this->email,
            'activation_code'   => $this->activation_code,
            'status'            => $this->status,
            'user_akses'          => Res_Acces::make($this->User_Acces),
        ];
    }
}
