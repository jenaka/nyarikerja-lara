<?php

namespace App\Http\Resources\User\Data;

use Illuminate\Http\Resources\Json\JsonResource;

class Res_Acces extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
