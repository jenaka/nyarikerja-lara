<?php

namespace App\Http\Controllers\cusAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PageController extends Controller
{
    public function opsi()
    {
        return view('Component/Register/opsi');
    }

    public function login_page()
    {
        return view('Component/login');
    }
}
