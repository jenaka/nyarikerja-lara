<?php

namespace App\Http\Controllers\cusAuth\Employe;

use App\Model\User\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;


class Register extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:employer');
    }

    public function registerPage()
    {
        return view('Component/Register/employer');
    }

    protected function validator(Requset $data)
    {
        return Validator::make($data, [
            'signup-username' => ['required', 'string', 'max:255'],
            'signup-email' => ['required', 'string', 'email', 'max:255', 'unique:Employer'],
            'signup-password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);
    }

    protected function create(Request $data)
    {
       $employer = Employer::create([
        'nama_lengkap' => $data['signup-username'],
        'email'        => $data['signup-email'],
        'password'     => Hash::make($data['signup-password']),
       ]);

        Auth::guard('employer')->attempt(['email'=>$data['signup-email'],'password'=>$data['signup-password']]);
        return redirect()->route('home-emp'); 
    }
}
