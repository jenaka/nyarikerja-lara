<?php

namespace App\Http\Controllers\cusAuth\Employe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;

class LoginController extends Controller
{

  public function login_emp(Request $request)
  {
    $validate = Validator::make($request->all(),[
      'email' => 'required',
      'password' => 'required'
    ]);

    if ($validate->fails()) {
      return back()->withErrors($validate)->withInput();
    }

    $creds = $request->only('email','password');

    if (Auth::guard('employer')->attempt($creds,$request->remember)) {
      return redirect()->intended(route('home-emp'));
    }else {
      return redirect()->back()->withInput($request->only('email','remember'));
    }

  }

  public function logoutEmployer(Request $request)
  {
    Auth::logout();
    return redirect('/');
  }
}
