<?php

namespace App\Http\Controllers\AdminOperator\page;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->id_akses == 1) {
            return view('AdminOperator/Admin/home');
        } else {
            return view('AdminOperator/Admin/OpHome');
        }
    }

    //Halaman lain-lain 

    #KUALIFIKASI PEKERJAAN
    public function Kualifikasi()
    {
        return view('AdminOperator/Admin/crud/kualifikasi/kualifikasi');
    }

    #WILAYAH
    public function provinsi()
    {
        return view('AdminOperator/Admin/crud/wilayah/provinsi');
    }

    public function kabupaten($id)
    {
        $data_id = $id; 
        return view('AdminOperator/Admin/crud/wilayah/kabupaten')->with('datid',$id);
    }

    public function kecamatan($id)
    {
        $data_id = $id; 
        return view('AdminOperator/Admin/crud/wilayah/kecamatan')->with('datid',$id);   
    }

    public function desa($id)
    {
        $data_id = $id; 
        return view('AdminOperator/Admin/crud/wilayah/desa')->with('datid',$id);   
    }


    #KEAHLIAN
    public function keahlian()
    {
        return view('AdminOperator/Admin/crud/keahlian/keahlian');
    }

    #PENGALAMAN
    public function pengalaman()
    {
        return view('AdminOperator/Admin/crud/pengalaman/pengalaman');
    }

    #GAJI PEKERJA
    public function gajiPekerjaan()
    {
        return view('AdminOperator/Admin/crud/gaji/gaji');
    }

    #TIPE PEKERJAAN
    public function TipePekerjaan()
    {
        return view('AdminOperator/Admin/crud/job_tipe/tipe');
    }

    #INDUSTRI
    public function Industry()
    {
        return view('AdminOperator/Admin/crud/industry/industri');
    }
}
