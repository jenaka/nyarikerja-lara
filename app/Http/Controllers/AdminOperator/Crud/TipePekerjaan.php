<?php

namespace App\Http\Controllers\AdminOperator\Crud;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Jobs\Job_Type;
use App\Http\Resources\Jobs\Res_Type;

class TipePekerjaan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job_Type $data)
    {
        $data = $data->all();
        return Res_Type::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Job_Type $data,Request $request)
    {
        $data = $data->create([
            'nama_tipe' => $request->nama_tipe,
            'slug'      => $request->nama_tipe 
        ]);
        
        if (!$data) {
            return response()->json([
                'message' => 'data tidak bisa di simpan (error)'
            ]); 
        } else {
            $response = new Res_Type($data);
            return response()->json($response,201); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Job_Type $data,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Job_Type $data,Request $request, $id)
    {
        $data = $data->where('id',$id)->update([
            'nama_tipe' => $request->nama_tipe,
            'slug'      => $request->nama_tipe 
        ]);

        if ($data) {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update'
            ]);
        } else {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update(ID atau data tidak di temukan)'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job_Type $data,$id)
    {
        $data = $data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message' => 'Data kualifikasi berhasil di hapus'
            ],201);
        } else {
            return response()->json([
                'message' => 'Data kualifikasi tidak berhasil di hapus(ID atau data tidak di temukan)'
            ],500);
        }
    }
}
