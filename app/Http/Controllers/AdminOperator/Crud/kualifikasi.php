<?php

namespace App\Http\Controllers\AdminOperator\Crud;

use auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Model
use App\Model\Jobs\Job_Qualification;
//Resources
use App\Http\Resources\Jobs\Res_Qualification;

class Kualifikasi extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Job_Qualification $data)
    {
        $data = $data->all();
        return Res_Qualification::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Job_Qualification $data,Request $request)
    {
        $data = $data->create([
            'nama_kualifikasi' => $request->input('nama_qual'),
            'slug' => $request->input('nama_qual')
        ]);
        
        if (!$data) {
            return response()->json([
                'message' => 'data kualifikasi tidak bisa di simpan (error)'
            ]); 
        } else {
            $response = new Res_Qualification($data);
            return response()->json($response,201); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Job_Qualification $data,Request $request, $id)
    {
        $data = $data->where('id',$id)->update([
            'nama_kualifikasi' => $request->input('nama_qual'),
            'slug' => $request->input('nama_qual')
        ]);

        if ($data) {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update'
            ]);
        } else {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update(ID atau data tidak di temukan)'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job_Qualification $data,$id)
    {
        $data = $data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message' => 'Data kualifikasi berhasil di hapus'
            ],201);
        } else {
            return response()->json([
                'message' => 'Data kualifikasi tidak berhasil di hapus(ID atau data tidak di temukan)'
            ],500);
        }
    }
}
