<?php

namespace App\Http\Controllers\AdminOperator\Crud;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
//model
use App\Model\User\User;
use App\Model\User\Data\User_Acces;
//resource JSON
use App\Http\Resources\User\User as Res_User;
use App\Http\Resources\User\Data\Res_Acces;

class pengguna extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //PAGE CONTROL
    #tambah Pengguna
    public function tambah_pengguna()
    {
        if (Auth::user()->id_akses == 1) {
            $acces = User_Acces::get();
            return view('AdminOperator/Admin/crud/pengguna/tambah')->with('user_acces',$acces);
        } else {
            return back();
        }        
    }

    #edit pengguna
    public function edit_pengguna($id)
    {
        if (Auth::user()->id_akses == 1) {
            $data = User::where('id',$id)->first();
            $acces = User_Acces::get();
            return view('AdminOperator/Admin/crud/pengguna/edit')->with('data',$data)->with('user_acces',$acces);
        } else {
            return back();
        }
    }

    #Data Pengguna
    public function data_pengguna()
    {
        if (Auth::user()->id_akses == 1) {
            return view('AdminOperator/Admin/crud/pengguna/data');
        } else {
            return back();
        }
    }

    #user Akses
    public function data_akses(User_Acces $data)
    {
        $data = $data->all();
        return Res_Acces::collection($data);
    }

    //----------------------------------------------------------------------------

    //DATA MANIPULATION
    public function data(User $data)
    {
        $data = $data->where('id','!=',Auth::user()->id)->get();
        return Res_User::collection($data);
    }

    public function store(Request $request)
    {
        if (Auth::user()->id_akses == 1) {
            $tempt_stat = 1;

            User::create([
                'nama'      => $request->nama, 
                'email'     => $request->email, 
                'password'  => Hash::make($request->password),
                'status'    => $tempt_stat,
                'username'  => $request->username,
                'id_akses'  => $request->id_akses,
                'api_token' => str_random(60),
            ]);

            return redirect(route('show_pengguna'));
        } else {
            return response()->json([
                'error'     => true,
                'message'   => 'Anda tidak memiliki akses untuk aksi ini.'
            ],402);
        }
    }

    public function update(Request $request, $id)
    {
        if (Auth::user()->id_akses == 1) {
        
            User::where('id',$id)->update([
                'nama'      => $request->nama, 
                'email'     => $request->email,
                'username'  => $request->username,
                'id_akses'  => $request->id_akses,
            ]);

            return redirect()->route('show_pengguna');
        } else {
            return response()->json([
                'error'     => true,
                'message'   => 'Anda tidak memiliki akses untuk aksi ini.'
            ],402);
        }
    }

    public function delete(Request $request, $id, User $user)
    {
        if (Auth::user()->id_akses == 1) {
            $user = $user->where('id',$id)->first();

            if (!$user) {
                return response()->json([
                    'message' => 'Data Tidak Ada.'
                ],500);
            }

            if ($user->delete()) {
                return response()->json([
                    'message' => 'data berhasil di hapus.'
                ],201);
            }else{
                return response()->json([
                    'message' => 'data tidak berhasil di hapus.'
                ],500);
            }
        } else {
            return response()->json([
                'error'     => true,
                'message'   => 'Anda tidak memiliki akses untuk aksi ini.'
            ],402);
        }
    }
    //---------------------------------------------------------------------
}
