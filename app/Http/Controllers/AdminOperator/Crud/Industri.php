<?php

namespace App\Http\Controllers\AdminOPerator\Crud;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Jobs\Res_Industry;
use App\Model\Jobs\Job_Industry;

class Industri extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job_Industry $data)
    {
        $data = $data->all();
        return Res_Industry::collection($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Job_Industry $data,Request $request)
    {
        $data = $data->create([
            'nama_industri' => $request->nama_indus,
            'slug'          => $request->nama_indus,
            'status'        => $request->status_indus,
        ]);
        
        if (!$data) {
            return response()->json([
                'message' => 'data tidak bisa di simpan (error)'
            ]); 
        } else {
            $response = new Res_Industry($data);
            return response()->json($response,201); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Job_Industry $data,Request $request, $id)
    {
        $data = $data->where('id',$id)->update([
            'nama_industri' => $request->nama_indus,
            'slug'          => $request->nama_indus,
        ]);

        if ($data) {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update'
            ]);
        } else {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update(ID atau data tidak di temukan)'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job_Industry $data,$id)
    {
        $data = $data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message' => 'Data kualifikasi berhasil di hapus'
            ],201);
        } else {
            return response()->json([
                'message' => 'Data kualifikasi tidak berhasil di hapus(ID atau data tidak di temukan)'
            ],500);
        }
    }
}
