<?php

namespace App\Http\Controllers\AdminOperator\Crud;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Jobs\Job_Salary;
use App\Http\Resources\Jobs\Res_Salary;

class GajiPekerjaan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job_Salary $data)
    {
        $data = $data->all();
        return Res_Salary::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Job_Salary $data,Request $request)
    {
        $data = $data->create([
            'judul'     => $request->judul,
            'jumlah'    => $request->jumlah,
            'slug'      => $request->judul,
        ]);
        
        if (!$data) {
            return response()->json([
                'message' => 'data tidak bisa di simpan (error)'
            ]); 
        } else {
            $response = new Res_Salary($data);
            return response()->json($response,201); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Job_Salary $data,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Job_Salary $data,Request $request, $id)
    {
        $data = $data->where('id_gaji',$id)->update([
            'judul'     => $request->judul,
            'jumlah'    => $request->jumlah,
        ]);

        if ($data) {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update'
            ]);
        } else {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update(ID atau data tidak di temukan)'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job_Salary $data,$id)
    {
        $data = $data->where('id_gaji',$id)->delete();

        if ($data) {
            return response()->json([
                'message' => 'Data kualifikasi berhasil di hapus'
            ],201);
        } else {
            return response()->json([
                'message' => 'Data kualifikasi tidak berhasil di hapus(ID atau data tidak di temukan)'
            ],500);
        }
    }
}
