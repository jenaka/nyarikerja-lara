<?php

namespace App\Http\Controllers\AdminOPerator\Crud;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//model
use App\Model\Jobs\Job_Skill;
//resouces
use App\Http\Resources\Jobs\Res_Skill;

class Keahlian extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job_Skill $data)
    {
        $data = $data->get();
        return Res_Skill::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Job_Skill $data,Request $request)
    {
       
        $data = $data->create([      
            'nama_skill'=> $request->nama_skill,
        ]);
        
        if ($data) {
            return response()->json([
                'message' => 'data berhasil di simpan'
            ],201);
        } else {
            return response()->json([
                'message' => 'data tidak berhasil di simpan'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Job_Skill $data,Request $request, $id)
    {
        $data=$data->where('id',$id)->update([
            'nama_skill'=> $request->nama_skill,
        ]);

        if ($data) {
            return response()->json(
                ['message'=>'data berhasil diperbaharui']
            );
        } else {
            return response()->json(
                ['message'=>'data tidak berhasil diperbaharui(data tidak ditemukan/error)']
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job_Skill $data,$id)
    {
        $data=$data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message'=>'data behasil dihapus'
            ]);
        } else {
            return response()->json([
                'message'=>'data tidak behasil dihapus'
            ]);
        }
    }
}
