<?php

namespace App\Http\Controllers\AdminOperator\Crud;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Model
use App\Model\Jobs\Job_Category;
//Resources
use App\Http\Resources\Jobs\Res_Category;

class Kategori extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job_Category $data)
    {
        $data = $data->all();
        return Res_Category::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Job_Category $data)
    {
        $data = $data->create([
            'nama' => $request->input('nama'),
            'slug' => $request->nama,
        ]);

        if (!$data) {
            return response()->json([
                'message'=>'data tidak bisa di simpan.'
            ],500);
        }else {
            $response = new Res_Category($data);
            return response()->json($response,201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Job_Category $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,Job_Category $data)
    {
        $update = $data->where('id_kategori',$id)->update([
            'nama' => $request->input('nama'),
        ]);

        if (!$update) {
            return response()->json([
                'message'=>'data tidak berhasil di update'
            ],500);
        } else {
            return response()->json([
                'message'=>'data berhasil di update, data kosong atau error'
            ],201);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Job_Category $data)
    {
        $data = $data->where('id_kategori',$id)->delete();

        if (!$data) {
            return response()->json([
                'message'=>'data tidak berhasil di hapus, data tidak ada atau error'
            ],500);
        } else {
            return response()->json([
                'message'=>'data berhasil berhasil di hapus'
            ],201);
        }
        
    }
}
