<?php

namespace App\Http\Controllers\AdminOperator\Crud\Wilayah;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//model
use App\Model\Area\Provincy;
//resource
use App\Http\Resources\Area\Provincy as res_prov;

class Provinsi extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Provincy $prov)
    {
        $prov = $prov->all();
        return res_prov::collection($prov);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Provincy $prov,Request $request)
    {
        $id = random_int(1,10);
        
        $prov = $prov->create([
            'name' => $request->nama_prov,
            'id' => $id,
        ]);
        
        if ($prov) {
            return response()->json([
                'message'=>'data provinsi berhasil di simpan'
            ],201);
        }else{
            return response()->json([
                'message'=>'data provinsi gagal di simpan'
            ],500);
        }
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Provincy $prov,$id)
    {
        $prov = $prov->where('id',$id)->first();
        return res_prov::make($prov);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Provincy $prov,Request $request, $id)
    {
        $prov = $prov->where('id',$id)->update([
            'name'=>$request->nama_prov
        ]);

        if ($prov) {
            return response()->json([
                'message' => 'data berhasil di update'
            ],201);
        } else {
            return response()->json([
                'message' => 'data gagal di update, Data tidak di temukan'
            ],402);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provincy $prov,$id)
    {
        $prov = $prov->where('id',$id)->delete();

        if ($prov) {
            return response()->json([
                'message' => 'data berhasil di hapus'
            ],201);
        } else {
            return response()->json([
                'message' => 'data tidak berhasil di hapus. data tidak di temukan'
            ],402);
        }
    }
}
