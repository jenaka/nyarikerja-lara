<?php

namespace App\Http\Controllers\AdminOperator\Crud\Wilayah;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//model 
use App\Model\Area\City;
//resource
use App\Http\Resources\Area\City as res_Kabupaten;

class Kabupaten extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(City $data,$id)
    {
        $data = $data->where('province_id',$id)->get();
        return res_Kabupaten::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(City $data,Request $request)
    {
        $min = 9471;
        $max = 9999;
        $set_id = random_int($min,$max);

        $data = $data->create([      
            'id'=>$set_id,
            'name'=> $request->nama_kab,
            'province_id'=> $request->prov_id,
        ]);
        
        if ($data) {
            return response()->json([
                'message' => 'data berhasil di simpan'
            ],201);
        } else {
            return response()->json([
                'message' => 'data tidak berhasil di simpan'
            ],500);
        }
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(City $data,$id)
    {
        $data = $data->where('id',$id)->first();
        return res_Kabupaten::make($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(City $data,Request $request, $id)
    {
        $data=$data->where('id',$id)->update([
            'name'=> $request->nama_kab,
        ]);

        if ($data) {
            return response()->json(
                ['message'=>'data berhasil diperbaharui']
            );
        } else {
            return response()->json(
                ['message'=>'data tidak berhasil diperbaharui(data tidak ditemukan/error)']
            );
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $data,$id)
    {
        $data=$data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message'=>'data behasil dihapus'
            ]);
        } else {
            return response()->json([
                'message'=>'data tidak behasil dihapus'
            ]);
        }
    }

}
