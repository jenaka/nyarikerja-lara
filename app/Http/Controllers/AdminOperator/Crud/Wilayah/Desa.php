<?php

namespace App\Http\Controllers\AdminOperator\Crud\Wilayah;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Model
use App\Model\Area\Villages;
//Resource
use App\Http\Resources\Area\Vilages as res_vilages;

class Desa extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Villages $data,$id)
    {
        $data = $data->where('district_id',$id)->get();
        return res_vilages::collection($data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Villages $data,Request $request)
    {
        
        $set_id = rand(1,110000);
        $data = $data->create([      
            'id'=>$set_id,
            'name'=> $request->nama_desa,
            'district_id'=> $request->kec_id,
        ]);
        
        if ($data) {
            return response()->json([
                'message' => 'data berhasil di simpan'
            ],201);
        } else {
            return response()->json([
                'message' => 'data tidak berhasil di simpan'
            ],500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Villages $data,$id)
    {
        $data = $data->where('id',$id)->get();
        return res_vilages::make($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Villages $data,Request $request, $id)
    {
        $data = $data->where('id','=',$id)->update([
            'name' => $request->nama_desa,
        ]);
        if ($data) {
            return response()->json(
                ['message'=>'data berhasil diperbaharui']
            );
        } else {
            return response()->json(
                ['message'=>'data tidak berhasil diperbaharui(data tidak ditemukan/error)']
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Villages $data,$id)
    {
        $data=$data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message'=>'data behasil dihapus'
            ]);
        } else {
            return response()->json([
                'message'=>'data tidak behasil dihapus'
            ]);
        }
    }
}
