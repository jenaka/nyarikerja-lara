<?php

namespace App\Http\Controllers\AdminOperator\Crud\Wilayah;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Model
use App\Model\Area\District;
//Resources
use App\Http\Resources\Area\District as res_district;

class Kecamatan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(District $data,$id)
    {
        $data = $data->where('regency_id',$id)->get();
        return res_district::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(District $data,Request $request)
    {
        $min = 3205310;
        $max = 9999999;
        $set_id = random_int($min,$max);

        $data = $data->create([      
            'id'=>$set_id,
            'name'=> $request->nama_kec,
            'regency_id'=> $request->kab_id,
        ]);
        
        if ($data) {
            return response()->json([
                'message' => 'data berhasil di simpan'
            ],201);
        } else {
            return response()->json([
                'message' => 'data tidak berhasil di simpan'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(District $data,$id)
    {
        $data = $data->where('id',$id)->first();
        return res_district::make($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(District $data,Request $request, $id)
    {
        $data=$data->where('id',$id)->update([
            'name'=> $request->nama_kec,
        ]);

        if ($data) {
            return response()->json(
                ['message'=>'data berhasil diperbaharui']
            );
        } else {
            return response()->json(
                ['message'=>'data tidak berhasil diperbaharui(data tidak ditemukan/error)']
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $data,$id)
    {
        $data=$data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message'=>'data behasil dihapus'
            ]);
        } else {
            return response()->json([
                'message'=>'data tidak behasil dihapus'
            ]);
        }
    }
}
