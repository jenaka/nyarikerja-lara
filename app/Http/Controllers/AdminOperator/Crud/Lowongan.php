<?php

namespace App\Http\Controllers\AdminOperator\Crud;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
//model
use app\Model\Jobs\Job_Post;
//Resources
use app\Http\Resources\Jobs\Res_Post;

class Lowongan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //PAGE CONTROL
    #KATEGORI
    public function kategori_lowongan()
    {
        return view('AdminOperator/Admin/crud/lowongan/kategori_lowongan');
    }

    #TAMBAH LOWONGAN
    public function tambah_lowongan_p()
    {
        return view('AdminOperator/Admin/crud/lowongan/tambah_lowongan');
    }

    #DATA LOKER
    public function data_loker()
    {
        return view('AdminOperator/Admin/crud/lowongan/data_lowongan');
    }

    //-----------------------------------------------------------------------

    public function edit_lowongan($id)
    {
        $data = Job_Post::where('id',$id)->first();
        return view();
    }

    public function data( Job_Post $data)
    {
        $data = $data->get();
        return Res_Post::collection();
    }

    //MANIPULATE
    public function store(Request $request)
    {

    }

    public function update(Jobs_Post $data,Request $request,$id)
    {

    }

    public function delete(Jobs_Post $data,Request $request,$id)
    {

    }
}
