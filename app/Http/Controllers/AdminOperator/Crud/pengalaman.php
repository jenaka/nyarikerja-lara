<?php

namespace App\Http\Controllers\AdminOperator\Crud;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Jobs\Job_Experience;
use App\Http\Resources\Jobs\Res_Experience;

class Pengalaman extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job_Experience $data)
    {
        $data = $data->all();
        return Res_Experience::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Job_Experience $data,Request $request)
    {
        $data = $data->create([
            'nama' => $request->input('nama_peng'),
        ]);
        
        if (!$data) {
            return response()->json([
                'message' => 'data tidak bisa di simpan (error)'
            ]); 
        } else {
            $response = new Res_Experience($data);
            return response()->json($response,201); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Job_Experience $data,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Job_Experience $data,Request $request, $id)
    {
        $data = $data->where('id',$id)->update([
            'nama' => $request->input('nama_peng')
        ]);

        if ($data) {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update'
            ]);
        } else {
            return response()->json([
                'message' => 'data kualifikasi berhasi di update(ID atau data tidak di temukan)'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job_Experience $data,$id)
    {
        $data = $data->where('id',$id)->delete();

        if ($data) {
            return response()->json([
                'message' => 'Data kualifikasi berhasil di hapus'
            ],201);
        } else {
            return response()->json([
                'message' => 'Data kualifikasi tidak berhasil di hapus(ID atau data tidak di temukan)'
            ],500);
        }
    }
}
