<?php

namespace App\Http\Controllers\Employer\page;

use Auth;
use DB;
use App\Model\Jobs\Job_Industry;
use App\Model\Area\Provincy;
use App\Model\Area\City;
use App\Model\User\Data\Company;
use App\Model\User\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employer');
    }

    public function index(){
        $status =   DB::table('pegawai')
                    ->leftJoin('perusahaan','pegawai.id_perusahaan','=','perusahaan.id_perusahaan')
                    ->where('pegawai.id','=',Auth::user()->id)
                    ->value('status');
        //dd($status);
        if ((Auth::user()->id_perusahaan == null) || ($status == null) || ($status == 0)) {
            $get_prov = Provincy::all();
            $get_city = City::all();
            $get_indu = Job_Industry::where('status',1)->get();
            return view('Component/Verification/emp_verification')
            ->with('prov',$get_prov)
            ->with('city',$get_city)
            ->with('indu',$get_indu);
        } else {
            return view('Employer/home');
        }
    }

    public function validation(Request $request)
    {
        $tempt = 1;
        //dd($request);
        Company::create([
            'nama_perusahaan'=>$request->nama_perusahaan,
            'logo_perusahaan'=>$request->logo_perusahaan,
            'web_perusahaan'=>$request->web_perusahaan,
            'id_jenis'=>$request->id_jenis,
            'deskripsi_perusahaan'=>$request->deskripsi_perusahaan,
            'slug_perusahaan'=>$request->nama_perusahaan,
            'status'=>$request->status,
            'id_provinsi'=>$request->id_provinsi,
            'id_kota'=>$request->id_kota,
            'alamat_perusahaan'=>$request->alamat_perusahaan,
            'id_perusahaan'=>$request->id_perusahaan,
        ]);

        Employer::where('id',Auth::user()->id)->update([
            'id_perusahaan' => $request->id_perusahaan
        ]);

        return redirect()->route('home-emp');
    }
}
