<?php

namespace App\Model\Area;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'kota';
    public $timestamps = false;
    protected $fillable = [
        'province_id','name','id'
    ];

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Post','id_kota','id_id');
    }

    public function Provincy()
    {
        return $this->belongsTo('App\Model\Area\Provincy','id','province_id');
    }

    public function District()
    {
        return $this->hasMany('App\Model\Area\District','regency_id','id');
    }
}
