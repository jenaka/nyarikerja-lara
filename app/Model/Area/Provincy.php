<?php

namespace App\Model\Area;

use Illuminate\Database\Eloquent\Model;

class Provincy extends Model
{
    protected $table = 'provinsi';
    public $timestamps = false;
    protected $fillable = [
        'id','name'
    ];

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Post','id_provinsi','id');
    }

    public function City ()
    {
        return $this->hasMany('App\Model\Area\City','province_id','id');
    }

}
