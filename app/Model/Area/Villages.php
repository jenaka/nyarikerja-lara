<?php

namespace App\Model\Area;

use Illuminate\Database\Eloquent\Model;

class Villages extends Model
{
    protected $table = 'villages';
    public $timestamps = false;
    protected $fillable = [
        'district_id','name','id'
    ];

    public function District()
    {
        return $this->belongsTo('App\Model\Area\District','id','district_id');
    }
}
