<?php

namespace App\Model\Area;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';
    public $timestamps = false;
    protected $fillable = [
        'regency_id','name','id'
    ];

    public function City()
    {
        return $this->belongsTo('App\Model\Area\City','id','regency_id');
    }

    public function Villages()
    {
        return $this->hasMany('App\Model\Area\Vilaages','district_id','id');
    }
}
