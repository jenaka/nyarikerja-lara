<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Type extends Model
{
    protected $table = 'job_tipe';
    public $timestamps = false;
    
    protected $fillable = [
        'nama_tipe','slug'
    ];

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Post','id_job_tipe','id');
    }
}
