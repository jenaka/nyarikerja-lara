<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Post extends Model
{
    protected $table = 'job_post';

    protected $fillable = [
        'job_title','job_slug',
        'id_kategori','id_sub_kategori',
        'id_perusahaan','id_provinsi',
        'id_gaji','pengalaman',
        'id_kualifikasi','id_kota',
        'id_job_tipe','job_deskripsi',
        'keahlian','status',
        'last_date','tgl',
    ];

    public function Job_Category()
    {
        return $this->hasOne('App\Model\Jobs\Jobs_Category','id_kategori','id_kategori');
    }

    public function Job_sub_category()
    {
        return $this->hasOne('App\Model\Jobs\Job_sub_category','id_sub_kategori','id_sub_kategori');
    }

    public function Company()
    {
        return $this->hasOne('App\Model\User\Data\Company','id_perusahaan','id_perusahaan');
    }

    public function Provincy()
    {
        return $this->hasONe('App\Model\Area\Provincy','id','id_provinsi');
    }

    public function Job_Salary()
    {
        return $this->hasOne('App\Model\Jobs\Job_Salary','id_gaji','id_gaji');
    }

    public function Job_Qualification()
    {
        return $this->hasOne('App\Model\Jobs\Job_Qualification','id','id_kualifikasi');
    }

    public function City()
    {
        return $this->hasOne('App\Model\Area\City','id','id_kota');
    }

    public function Job_Type()
    {
        return $this->hasOne('App\Model\Jobs\Job_Type','id','id_job_tipe');
    }
}
