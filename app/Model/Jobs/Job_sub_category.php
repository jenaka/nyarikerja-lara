<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_sub_category extends Model
{
    protected $table = 'job_sub_kategory';

    protected $fillable = [
        'id_sub_kategori','nama','slug'
    ];

    public function Job_Category()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Category','id','id_kategori');
    }

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Post','id_sub_kategori','id_sub_kategori');
    }
}
