<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Salary extends Model
{
    protected $table = 'job_gaji';
    public $timestamps = false;
    protected $fillable = [
        'judul','jumlah','slug'
    ];

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Post','id_gaji','id_gaji');
    }
}
