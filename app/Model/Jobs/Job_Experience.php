<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Experience extends Model
{
    protected $table = 'job_pengalaman';
    public $timestamps = false;
    protected $fillable = [
        'nama'
    ];
}
