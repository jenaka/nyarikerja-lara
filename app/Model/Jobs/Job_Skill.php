<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Skill extends Model
{
    protected $table = 'job_skill';
    public $timestamps = false;
    protected $fillable = [
        'nama_skill','id'
    ];
}
