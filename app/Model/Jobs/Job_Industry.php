<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Industry extends Model
{
    protected $table = 'job_indsutri';
    public $timestamps = false;
    
    protected $fillable = [
        'nama_industri','slug','status'
    ];
}
