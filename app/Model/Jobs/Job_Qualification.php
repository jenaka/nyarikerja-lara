<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Qualification extends Model
{
    protected $table = 'job_kualifikasi';
    public $timestamps = false;
    protected $fillable = [
        'nama_kualifikasi','slug'
    ];

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Post','id_kualifikasi','id');
    }
}
