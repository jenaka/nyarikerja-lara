<?php

namespace App\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class Job_Category extends Model
{
    protected $table = 'job_kategori';
    public $timestamps = false;
    protected $fillable = [
        'nama',
        'slug'
    ];

    public function Job_sub_category()
    {
        return $this->hasMany('App\Model\Jobs\Job_Category','id_kategori','id_kategori');
    }

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs\Job_Post','id_kategori','id_kategori');
    }
}
