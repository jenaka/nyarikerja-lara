<?php

namespace App\Model\User\Data;

use Illuminate\Database\Eloquent\Model;

class Seeker_Pendidikan extends Model
{
    protected $table = 'seeker_pendidikan';

    protected $fillable = [
        'id_seeker','nama_gelar',
        'nama_lembaga','alamat_lembaga',
    ];

    public function Seeker()
    {
        return $this->belongsTo('App\Model\User\Seeker','id_seeker','id_seeker');
    }
}
