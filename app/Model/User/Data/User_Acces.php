<?php

namespace App\Model\User\Data;

use Illuminate\Database\Eloquent\Model;

class User_Acces extends Model
{
    protected $table = 'user_akses';

    protected $fillable = [
        'nama'
    ];

    public function User()
    {
        return $this->belongsTo('App\Model\User\User','id_akses','id');
    }
}
