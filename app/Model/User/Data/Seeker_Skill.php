<?php

namespace App\Model\User\Data;

use Illuminate\Database\Eloquent\Model;

class Seeker_Skill extends Model
{
    protected $table = 'seeker_skill';

    protected $fillable = [
        'id_seeker','nama_skill'
    ];

    public function Seeker()
    {
        return $this->belongsTo('App\Model\User\Seeker','id_seeker','id_seeker');
    }
}
