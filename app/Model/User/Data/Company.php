<?php

namespace App\Model\User\Data;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'perusahaan';
    public $timestamps = false;
    protected $fillable = [
        'nama_perusahaan','logo_perusahaan',
        'web_perusahaan','id_jenis',
        'deskripsi_perusahaan','slug_perusahaan',
        'status','id_provinsi',
        'id_kota','alamat_perusahaan','id_perusahaan'
    ];

    public function Employer()
    {
        return $this->belongsTo('App\Model\User\Employer','id_perusahaan','id_perusahaan');
    }

    public function Job_Post()
    {
        return $this->belongsTo('App\Model\Jobs','id_perusahaan','id_perusahaan');
    }
}
