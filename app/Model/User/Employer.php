<?php

namespace App\Model\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employer extends Authenticatable
{
    use Notifiable;
    protected $guard = 'employer';
    public $timestamps = false;
    const CREATED_AT = 'tgl_join';
    protected $table = 'pegawai';

    protected $fillable = [
        'id_perusahaan','email',
        'password','nama_lengkap',
        'no_ktp','no_hp',
        'tgl_join',
    ];

    protected $hidden = [
        'password','remember_token'
    ];

    public function Company()
    {
        return $this->hasOne('App\Model\User\Data\Company','id_perusahaan','id_perusahaan');
    }
}
