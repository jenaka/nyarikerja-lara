<?php

namespace App\Model\User;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pelamar extends Authenticatable
{
  
    use Notifiable;
    public $timestamps = false;
    protected $table = 'seeker';
    
    protected $fillable = [
        'nama','email','password',
        'api_token','foto','tmp_lahir',
        'tgl_lahir','jk','no_hp',
        'agama','kewarganegaraan','status',
        'id_kecamatan','id_kelurahan','alamat',
        'remember_token'
    ];

    protected $hidden = [
        'password','remember_token'
    ];
}
