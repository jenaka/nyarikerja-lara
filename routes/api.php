<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Admin & Operator

Route::middleware('auth:api')->group(function(){
    //Pengguna
    Route::get('/pengguna/data-akses','AdminOperator\crud\pengguna@data_akses');
    Route::get('/pengguna/data','AdminOperator\Crud\pengguna@data');
    Route::delete('/pengguna/data-delete/{id}','AdminOperator\crud\pengguna@delete');
    Route::post('/Pengguna/Tambah','AdminOperator\Crud\pengguna@store');

    //LOKER
    #kategori
    Route::get('/kategori','AdminOperator\Crud\Kategori@index');
    Route::post('/kategori/save','AdminOperator\Crud\Kategori@store');
    Route::put('/kategori/update/{id}','AdminOperator\Crud\Kategori@update');
    Route::delete('/kategori/delete/{id}','AdminOperator\Crud\Kategori@destroy');

    //PERUSAHAAN

    //JOBS SEEKER

    //INDUSTRY
    Route::get('/Industri','AdminOperator\Crud\Industri@index');
    Route::post('/Industri/save','AdminOperator\Crud\Industri@store');
    Route::put('/Industri/update/{id}','AdminOperator\Crud\Industri@update');
    Route::delete('/Industri/delete/{id}','AdminOperator\Crud\Industri@destroy');

    //T.JOBS
    Route::get('/Tipe','AdminOperator\Crud\TipePekerjaan@index');
    Route::post('/Tipe/save','AdminOperator\Crud\TipePekerjaan@store');
    Route::put('/Tipe/update/{id}','AdminOperator\Crud\TipePekerjaan@update');
    Route::delete('/Tipe/delete/{id}','AdminOperator\Crud\TipePekerjaan@destroy');

    //GAJI
    Route::get('/Gaji','AdminOperator\Crud\GajiPekerjaan@index');
    Route::post('/Gaji/save','AdminOperator\Crud\GajiPekerjaan@store');
    Route::put('/Gaji/update/{id}','AdminOperator\Crud\GajiPekerjaan@update');
    Route::delete('/Gaji/delete/{id}','AdminOperator\Crud\GajiPekerjaan@destroy');

    //PENGALAMAN
    Route::get('/Pengalaman','AdminOperator\Crud\Pengalaman@index');
    Route::post('/Pengalaman/save','AdminOperator\Crud\Pengalaman@store');
    Route::put('/Pengalaman/update/{id}','AdminOperator\Crud\Pengalaman@update');
    Route::delete('/Pengalaman/delete/{id}','AdminOperator\Crud\Pengalaman@destroy');

    //SKILL
    Route::get('/keahlian','AdminOperator\Crud\Keahlian@index');
    Route::post('/keahlian/save','AdminOperator\Crud\Keahlian@store');
    Route::put('/keahlian/update/{id}','AdminOperator\Crud\Keahlian@update');
    Route::delete('/keahlian/delete/{id}','AdminOperator\Crud\Keahlian@destroy');

    //KUALIFIKASI
    Route::get('/kualifikasi','AdminOperator\Crud\Kualifikasi@index');
    Route::post('/kualifikasi/save','AdminOperator\Crud\Kualifikasi@store');
    Route::put('/kualifikasi/update/{id}','AdminOperator\Crud\Kualifikasi@update');
    Route::delete('/kualifikasi/delete/{id}','AdminOperator\Crud\Kualifikasi@destroy');

    //WILAYAH
    #provinsi
    Route::get('/wilayah/provinsi','AdminOperator\Crud\Wilayah\Provinsi@index');
    Route::get('/wilayah/provinsi/{id}','AdminOperator\Crud\Wilayah\Provinsi@show');
    Route::post('/wilayah/provinsi/save','AdminOperator\Crud\Wilayah\Provinsi@store');
    Route::put('/wilayah/provinsi/update/{id}','AdminOperator\Crud\Wilayah\Provinsi@update');
    Route::delete('/wilayah/provinsi/delete/{id}','AdminOperator\Crud\Wilayah\Provinsi@destroy');

    #KABUPATEN
    Route::get('/wilayah/kabupaten/{id}','AdminOperator\Crud\Wilayah\Kabupaten@index');
    Route::get('/wilayah/kabupaten/sh/{id}','AdminOperator\Crud\Wilayah\Kabupaten@show');
    Route::post('/wilayah/kabupaten/save','AdminOperator\Crud\Wilayah\Kabupaten@store');
    Route::put('/wilayah/kabupaten/update/{id}','AdminOperator\Crud\Wilayah\Kabupaten@update');
    Route::delete('/wilayah/kabupaten/delete/{id}','AdminOperator\Crud\Wilayah\Kabupaten@destroy');

    #KECAMATAN
    Route::get('/wilayah/kecamatan/{id}','AdminOperator\Crud\Wilayah\Kecamatan@index');
    Route::get('/wilayah/kecamatan/sh/{id}','AdminOperator\Crud\Wilayah\Kecamatan@show');
    Route::post('/wilayah/kecamatan/save','AdminOperator\Crud\Wilayah\Kecamatan@store');
    Route::put('/wilayah/kecamatan/update/{id}','AdminOperator\Crud\Wilayah\Kecamatan@update');
    Route::delete('/wilayah/kecamatan/delete/{id}','AdminOperator\Crud\Wilayah\Kecamatan@destroy');

    #DESA
    Route::get('/wilayah/desa/{id}','AdminOperator\Crud\Wilayah\Desa@index');
    Route::post('/wilayah/desa/save','AdminOperator\Crud\Wilayah\Desa@store');
    Route::put('/wilayah/desa/update/{id}','AdminOperator\Crud\Wilayah\Desa@update');
    Route::delete('/wilayah/desa/delete/{id}','AdminOperator\Crud\Wilayah\Desa@destroy');
});

//Employer
Route::middleware('auth:api-employe')->group(function(){

});

//Seeker
Route::middleware('auth:api-seeker')->group(function(){

});