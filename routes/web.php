<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/registrasi','cusAuth\PageController@opsi')->name('daftar');
Route::get('/user/login','cusAuth\PageController@login_page')->name('loginSec');

//autentifikasi perusahaan & pelamar
Route::prefix('perusahaan')->group(function(){
    //regis form
    Route::get('/registrasi','cusAuth\Employe\Register@registerPage')->name('employerRegis');
    Route::post('/registrasi/perusahaan/reg','cusAuth\Employe\Register@create')->name('emp_save');
    
    //login post
    Route::post('/login/validation','cusAuth\Employe\LoginController@login_emp')->name('login-perusahaan');
    Route::post('/logout','cusAuth\Employe\LoginController@logoutEmployer')->name('logout-perusahaan');
});

Route::prefix('seeker')->group(function(){
    //registrasi form 
    Route::get('/registrasi','cusAuth\Seeker\Register@register_page')->name('SeekerRegis');
    Route::post('/registrasi/seeker','cusAuth\Seeker\Register@create')->name('seeker_save');

    //login post
    Route::post('/login/validation','cusAuth\Seeker\LoginController@login_seeker')->name('login-seeker');
    Route::post('/logout','cusAuth\Seeker\LoginController@logout_seeker')->name('logout-seeker');
});

//Route Group ter-authentifikasi

//Admin & Operator
Route::group(['prefix'=>'admin','middleware'=> 'auth'],function(){
    //index
    Route::get('/', 'AdminOperator\page\HomeController@index')->name('home');

    //Pengguna
    Route::get('/Pengguna/Tambah','AdminOperator\Crud\pengguna@tambah_pengguna')->name('tambah_pengguna_p');
    Route::get('/Pengguna/Data','AdminOperator\Crud\pengguna@data_pengguna')->name('show_pengguna');
    Route::get('/Pengguna/Edit/{id}','AdminOperator\Crud\pengguna@edit_pengguna')->name('edit_pengguna');
    Route::post('/Pengguna/Update/{id}','AdminOperator\Crud\pengguna@update')->name('update_pengguna');
    Route::post('/Pengguna/post','AdminOperator\Crud\pengguna@store')->name('pengguna_post');

    //LOKER
    Route::get('/Lowongan/kategori','AdminOperator\Crud\Lowongan@kategori_lowongan')->name('kategori_job');
    Route::get('/Lowongan/Tambah','AdminOperator\Crud\Lowongan@tambah_lowongan_p')->name('tambah_lowongan');
    Route::get('/Lowongan/Data','AdminOperator\Crud\Lowongan@data_loker')->name('data_loker');
      
    //T.PEKERJAAN
    Route::get('/TipePekerjaan','AdminOperator\page\HomeController@TipePekerjaan')->name('tipe');

    //INDUSTRI
    Route::get('/Industri','AdminOperator\page\HomeController@Industry')->name('industri');
    
    //GAJI P
    Route::get('/Gaji','AdminOperator\page\HomeController@gajiPekerjaan')->name('gaji');

    //PENGALAMAN    
    Route::get('/Pengalaman','AdminOperator\page\HomeController@pengalaman')->name('pengalaman');

    //KEAHLIAN
    Route::get('/Keahlian','AdminOperator\page\HomeController@keahlian')->name('keahlian');

    //KUALIFIKASI
    Route::get('/Kualifikasi','AdminOperator\page\HomeController@Kualifikasi')->name('kualifikasi');

    //WILAYAH
    Route::get('/Wilayah','AdminOperator\page\HomeController@provinsi')->name('wilayah');
    Route::get('/Wilayah/Kabupaten/{id}','AdminOperator\page\HomeController@kabupaten')->name('kabupaten');
    Route::get('/Wilayah/Kecamatan/{id}','AdminOperator\page\HomeController@kecamatan')->name('kecamatan');
    Route::get('/Wilayah/Desa/{id}','AdminOperator\page\HomeController@desa')->name('desa');

    //JOBS SEEKER
});
//------------------------------------------------------------------------------


//Employe
Route::group(['prefix'=>'perusahaan','middleware'=>'auth:employer'],function(){
    //Main Page
    Route::get('/', 'Employer\page\HomeController@index')->name('home-emp');
    
    //validation
    Route::post('/validation/send','Employer\page\HomeController@validation')->name('val_post');
});
//------------------------------------------------------------------------------

